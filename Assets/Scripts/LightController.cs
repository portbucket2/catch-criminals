using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    [SerializeField] Material greenLight;
    [SerializeField] Material redLight;
    [SerializeField] Material whiteLight;

    [SerializeField] List<Light> lights;
    [SerializeField] List<MeshRenderer> lightBodies;
    void Start()
    {
        int max = lights.Count;
        for (int i = 0; i < max; i++)
        {
            lights[i].color = Color.white;
            lightBodies[i].material = whiteLight;
        }
    }

    public void LightUpRed(int index)
    {
        lights[index].color = Color.red;
        lightBodies[index].material = redLight;
    }
    public void LightUpGreen(int index)
    {
        lights[index].color = Color.green;
        lightBodies[index].material = greenLight;
    }
}

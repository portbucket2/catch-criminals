using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{

    [SerializeField] Button suspectZero;
    [SerializeField] Button suspectOne;
    [SerializeField] Button suspectTwo;
    [SerializeField] Button suspectThree;
    [SerializeField] Button suspectFour;

    [SerializeField] Button catchButton;

    [SerializeField] GameObject levelEndPanel;
    [SerializeField] Button nextLevelButton;
    [SerializeField] TextMeshProUGUI levelHint;

    GameController gameController;
    GameManager gameManager;
    AnalyticsController analytics;

    void Start()
    {
        gameController = GameController.GetController();
        gameManager = GameManager.GetManager();
        analytics = AnalyticsController.GetController();

        suspectZero.onClick.AddListener(delegate { SelectSuspect(0); });
        suspectOne.onClick.AddListener(delegate { SelectSuspect(1); });
        suspectTwo.onClick.AddListener(delegate { SelectSuspect(2); });
        suspectThree.onClick.AddListener(delegate { SelectSuspect(3); });
        suspectFour.onClick.AddListener(delegate { SelectSuspect(4); });

        catchButton.onClick.AddListener(delegate { CatchCriminal(); catchButton.gameObject.SetActive(false); });


        nextLevelButton.onClick.AddListener(delegate {
            gameManager.GotoNextStage();
        });

        catchButton.gameObject.SetActive(false);
    }

    void SelectSuspect(int index)
    {
        Debug.Log("suspect selected : " + index);
        gameController.GetSceneController().HighlightSuspect(index);
    }
    void CatchCriminal()
    {
        gameController.GetSceneController().CatchCriminal();
    }

    public void LevelComplete()
    {
        levelEndPanel.SetActive(true);
    }
    public void SetLevelHint(string hint)
    {
        levelHint.text = hint;
    }
    public void EnableCatchButton()
    {
        catchButton.gameObject.SetActive(true);
    }
}

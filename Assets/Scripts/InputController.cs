using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] float hoverOffset;
    [Header("Debug")]
    [SerializeField] bool inputEnabled = false;
    [SerializeField] Transform grabbedItem;
    [SerializeField] Camera camera;
    [SerializeField] Grabable currentGrabable;

    readonly string GRABABLE = "grabable";
    readonly string WALL = "wall";

    public float smoothTime = 100f;
    private Vector3 velocity = Vector3.zero;
    GameController gameController;

    void Start()
    {
        gameController = GameController.GetController();
        camera = gameController.GetCamera();
    }
    Ray ray;
    RaycastHit hit;
    // Update is called once per frame
    void Update()
    {
        if (!inputEnabled)
            return;

		if (Input.GetMouseButtonDown(0))
		{
            ray = camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray,out hit, 200f))
            {
                if (hit.transform.CompareTag(GRABABLE))
                {
                    grabbedItem = hit.transform;
                    currentGrabable = grabbedItem.GetComponent<Grabable>();
                }
            }
		}
        if (Input.GetMouseButton(0))
        {
            if (grabbedItem)
            {
                ray = camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 200f))
                {
                    if (hit.transform.CompareTag(WALL))
                    {
                        Vector3 target = hit.point + Vector3.up * hoverOffset;
                        //grabbedItem.position = target;
                        grabbedItem.position = Vector3.Lerp(transform.position, target, Time.deltaTime * smoothTime);
                        currentGrabable.LookAtSuspects();
                    }
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (grabbedItem)
            {
                grabbedItem.GetComponent<Grabable>().ResetPosition();
                grabbedItem = null;
            }
        }
    }

    public void EnableInput() { inputEnabled = true; }
    public void DisableInput() { inputEnabled = false; }
}

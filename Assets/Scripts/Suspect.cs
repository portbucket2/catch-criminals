using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suspect : MonoBehaviour
{

    [SerializeField] bool isMale = false;
    [Header("Debug : ")]
    [Header("Expression heads")]
    [SerializeField] GameObject fearedHead;
    [SerializeField] GameObject happyHead;
    [SerializeField] GameObject idleHead;
    [SerializeField] GameObject laughingHead;
    [SerializeField] GameObject sadhead;
    [SerializeField] GameObject scaredHead;

    readonly int LOOKAWAY = Animator.StringToHash("LookAway");
    readonly int FLINCH = Animator.StringToHash("Flinch");
    readonly int COVEREAR = Animator.StringToHash("CoverEar");
    readonly int ARRESTED = Animator.StringToHash("Arrested");
    readonly int HAPPY = Animator.StringToHash("Happy");
    readonly int LOL = Animator.StringToHash("LoL");
    readonly int SNEEZE = Animator.StringToHash("Sneeze");
    readonly int FEAR = Animator.StringToHash("Fear");
    readonly int IDLE = Animator.StringToHash("Idle"); // 5 idles
    readonly int WALKMALE = Animator.StringToHash("WalkMale"); 
    readonly int WALKFEMALE = Animator.StringToHash("WalkFemale"); 
    readonly int IDLEBLEND = Animator.StringToHash("IdleBlend"); 

    Animator animator;

    private void Awake()
    {
        animator = transform.parent.parent.GetComponent<Animator>();
        InitHeads();
    }
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InitHeads()
    {
        if (transform.childCount != 6)
            return;

        fearedHead = transform.GetChild(0).gameObject;
        happyHead = transform.GetChild(1).gameObject;
        idleHead = transform.GetChild(2).gameObject;
        laughingHead = transform.GetChild(3).gameObject;
        sadhead = transform.GetChild(4).gameObject;
        scaredHead = transform.GetChild(5).gameObject;


        fearedHead.gameObject.SetActive(false);
        happyHead.gameObject.SetActive(false);
        idleHead.gameObject.SetActive(false);
        laughingHead.gameObject.SetActive(false);
        sadhead.gameObject.SetActive(false);
        scaredHead.gameObject.SetActive(false);

        idleHead.gameObject.SetActive(true);
    }
    public Animator GetAnimator() { return animator; }
    public void PlayAnimation(SuspectAnimations anims)
    {
        switch (anims)
        {
            case SuspectAnimations.LOOKAWAY:
                PlayLookAway();
                break;
            case SuspectAnimations.FLINCH:
                PlayFlinch();
                break;

            case SuspectAnimations.COVEREAR:
                PlayCoverEar();
                break;

            case SuspectAnimations.ARRESTED:
                PlayArrested();
                break;

            case SuspectAnimations.HAPPY:
                PlayHappy();
                break;

            case SuspectAnimations.LOL:
                PlayLoL();
                break;

            case SuspectAnimations.SNEEZE:
                PlaySneeze();
                break;

            case SuspectAnimations.FEAR:
                PlayFear();
                break;

            case SuspectAnimations.IDLE:
                PlayIdle();
                break;

            case SuspectAnimations.WALK:
                PlayWalk();
                break;

            default:
                PlayIdle();
                break;
        }
    }


    void PlayLookAway()
    {
        animator.SetTrigger(LOOKAWAY);
        DeactivateAllFaces();
        scaredHead.gameObject.SetActive(true);
    }
    void PlayFlinch()
    {
        animator.SetTrigger(FLINCH);
        DeactivateAllFaces();
        laughingHead.gameObject.SetActive(true);
    }
    void PlayCoverEar()
    {
        animator.SetTrigger(COVEREAR);
        DeactivateAllFaces();
        scaredHead.gameObject.SetActive(true);
    }
    void PlayArrested()
    {
        animator.SetTrigger(ARRESTED);
        DeactivateAllFaces();
        sadhead.gameObject.SetActive(true);
    }
    void PlayHappy()
    {
        animator.SetTrigger(HAPPY);
        DeactivateAllFaces();
        happyHead.gameObject.SetActive(true);
    }
    void PlayLoL()
    {
        animator.SetTrigger(LOL);
        DeactivateAllFaces();
        laughingHead.gameObject.SetActive(true);
    }
    void PlaySneeze()
    {
        animator.SetTrigger(SNEEZE);
        DeactivateAllFaces();
        laughingHead.gameObject.SetActive(true);
    }
    void PlayFear()
    {
        animator.SetTrigger(FEAR);
        DeactivateAllFaces();
        scaredHead.gameObject.SetActive(true);
    }
    void PlayIdle()
    {
        int rand = Random.Range(0, 6);
        animator.SetFloat(IDLEBLEND, rand);
        animator.SetTrigger(IDLE);
        DeactivateAllFaces();
        idleHead.gameObject.SetActive(true);
    }
    void PlayWalk()
    {
        if (isMale)
        {
            animator.SetTrigger(WALKMALE);
            DeactivateAllFaces();
            idleHead.gameObject.SetActive(true);
        }
        else
        {
            animator.SetTrigger(WALKFEMALE);
            DeactivateAllFaces();
            idleHead.gameObject.SetActive(true);
        }
        
    }



    void DeactivateAllFaces()
    {
        fearedHead.gameObject.SetActive(false);
        happyHead.gameObject.SetActive(false);
        idleHead.gameObject.SetActive(false);
        laughingHead.gameObject.SetActive(false);
        sadhead.gameObject.SetActive(false);
        scaredHead.gameObject.SetActive(false);
    }
}



public enum SuspectAnimations {
    LOOKAWAY,
    FLINCH,
    COVEREAR,
    ARRESTED,
    HAPPY,
    LOL,
    SNEEZE,
    FEAR,
    IDLE,
    WALK
}

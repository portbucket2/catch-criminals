using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SceneController : MonoBehaviour
{
    [SerializeField] GameObject suspectPrefab;
    [SerializeField] int maxSuspects = 12;
    [SerializeField] GameDataSheet data;

    [SerializeField] Transform trayLeft;
    [SerializeField] Transform trayRight;
    [SerializeField] Vector3 trayOffset;
    [SerializeField] ParticleSystem confettiParticle;

    [Header("COP : ")]
    [SerializeField] Transform cop;
    [SerializeField] Animator copAnimator;
    [SerializeField] Vector3 arrestOffset;
    [SerializeField] Vector3 copOffset;

    [Header("Debug : ")]
    [SerializeField] int currentLevel;
    [SerializeField] LevelData levelData;
    [SerializeField] List<SuspectController> suspects;

    GameController gameController;
    LightController lights;

    WaitForSeconds WAITONE = new WaitForSeconds(4f);

    readonly int WALK = Animator.StringToHash("WalkMale");
    readonly int POLICEWALK = Animator.StringToHash("policeWalk");
    readonly int ARRESTED = Animator.StringToHash("Arrested");

    SuspectController culprit;
    SuspectController selectedSuspect;

    AnalyticsController analytics;
    GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.GetManager();
        analytics = AnalyticsController.GetController();
        if (gameManager)
            currentLevel = gameManager.GetlevelCount();

       levelData = data.levelDatas[currentLevel];
        gameController = GameController.GetController();
        lights = gameController.GetLightController();
        SpawnSuspects();
        gameController.GetUI().SetLevelHint(levelData.levelHint);
        if(analytics)
            analytics.LevelStarted();
    }

    void SpawnSuspects()
    {
        suspects = new List<SuspectController>();
        List<Vector3> pos = new List<Vector3>();
        Vector3 offset = new Vector3(1f,0f,0f);
        Vector3 Main = new Vector3(-2f,0.1f,-0.93f);
        Vector3 start = new Vector3(-5f,0.1f,-0.93f);
        int max = 5;
        for (int i = 0; i < max; i++)
        {
            pos.Add(Main + (offset * i));
            GameObject gg = Instantiate(suspectPrefab, start + (offset * i), suspectPrefab.transform.rotation);
            suspects.Add(gg.GetComponent<SuspectController>());
        }
        SetSuspects();

        for (int i = 0; i < max; i++)
        {
            suspects[i].WalkTo(pos[i]);
        }
    }
    void SetSuspects()
    {
        int max = 5;
        int rand = Random.Range(0, maxSuspects);
        //Debug.Log("suspect start :  " + rand);
        for (int i = 0; i < max; i++)
        {
            if (rand < maxSuspects)
            {
                suspects[i].SetSuspect(rand, i, levelData.guiltyAnimation, levelData.innocentAnimation, levelData.commonAnimation);
            }
            else
            {
                rand = 0;
                suspects[i].SetSuspect(rand, i, levelData.guiltyAnimation, levelData.innocentAnimation, levelData.commonAnimation);
            }
            rand++;
        }

        SetGuilty();
    }

    void SetGuilty()
    {
        int max = suspects.Count;
        int rand = Random.Range(0, max);
        suspects[rand].SetGuilty();
        SetProps();
    }
    void SetProps()
    {
        GameObject g1 = Instantiate(levelData.propPrefab);
        GameObject g2 = Instantiate(levelData.propPrefab);

        Grabable prop1 = g1.GetComponent<Grabable>();
        Grabable prop2 = g2.GetComponent<Grabable>();

        if (Random.Range(0f, 1f) > 0.5f) 
        {
            prop1.transform.position = trayLeft.position + trayOffset;
            prop2.transform.position = trayRight.position + trayOffset;
        }
        else
        {
            prop1.transform.position = trayRight.position + trayOffset;
            prop2.transform.position = trayLeft.position + trayOffset;
        }

        prop1.SetGrabable(true, levelData.mainProp);
        prop2.SetGrabable(false, levelData.extraProp);
    }

    

    void LevelComplete()
    {
        if (analytics)
            analytics.LevelCompleted();

        confettiParticle.Play();
        int max = suspects.Count;
        for (int i = 0; i < max; i++)
        {
            lights.LightUpRed(i);
        }
        Debug.LogError("Level Complete!! ");
        StartCoroutine(LevelEndRoutine());
    }

    IEnumerator LevelEndRoutine()
    {
        gameController.GetInput().DisableInput();
        ArrestRoutine();
        yield return WAITONE;
        //show level end ui
        gameController.GetUI().LevelComplete();
    }

    void ArrestRoutine()
    {
        copAnimator.SetTrigger(WALK);
        culprit.PlayAnimation(SuspectAnimations.LOOKAWAY);
        cop.DOMove(culprit.transform.position + copOffset, 3f).SetEase(Ease.Linear).OnComplete(ReachedCulprit);
        culprit.transform.DOMove(culprit.transform.position + arrestOffset, 1f).SetEase(Ease.OutSine);
        culprit.transform.DORotate(new Vector3(0f, -90f, 0f), 1f).SetEase(Ease.InSine);
        
    }
    void ReachedCulprit()
    {
        copAnimator.SetTrigger(POLICEWALK);
        culprit.GetSuspect().GetAnimator().SetTrigger(ARRESTED);
        cop.DOMove(Vector3.right * -10f, 6f).SetEase(Ease.Linear);
        culprit.transform.DOMove(Vector3.right * -10f, 6f).SetEase(Ease.Linear);
    }

    public void CrimeCheck(int index)
    {
        if (suspects[index].IsGuilty())
        {
            culprit = suspects[index];
            LevelComplete();
            lights.LightUpGreen(index);
        }
        else
        {
            lights.LightUpRed(index);
        }
    }

    public void HighlightSuspect(int index)
    {
        int max = suspects.Count;
        for (int i = 0; i < max; i++)
        {
            suspects[i].DeselectSuspect();
        }
        suspects[index].SelectSuspect();
        selectedSuspect = suspects[index];

        gameController.GetUI().EnableCatchButton();
    }
    public void CatchCriminal()
    {
        if (!selectedSuspect)
            return;

        int index = selectedSuspect.GetIndex();
        CrimeCheck(index);
    }
}

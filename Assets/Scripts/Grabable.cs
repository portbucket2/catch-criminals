using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Grabable : MonoBehaviour
{
    [SerializeField] Transform propHolder;
    [SerializeField] List<GameObject> props;
    [Header("DebugL: ")]
    [SerializeField] bool isMain = false;
    [SerializeField] bool touched = false;
    [SerializeField] Props propType;
    [SerializeField] Transform currentProp;
    [SerializeField] Vector3 currentPropRot;
    [SerializeField] Transform currentSuspect;
    Vector3 startPosition;
    readonly string SUSPECT = "suspect";
    
    void Start()
    {
        startPosition = transform.position;
    }

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(SUSPECT))
        {
            if (!touched)
            {
                touched = true;
                other.GetComponent<SuspectController>().StartInteraction(isMain);
                currentSuspect = other.transform;
            }           
        }
        else
        {
            touched = false;
            currentSuspect = null;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        touched = false;
    }
    public void LookAtSuspects()
    {
        if (currentSuspect)
            currentProp.LookAt(currentSuspect.position + Vector3.up * 1.5f);
    }
    public void ResetPosition()
    {
        transform.DOMove(startPosition, 0.3f);
        currentProp.localEulerAngles = currentPropRot;
    }
    public void SetGrabable(bool _isMain, Props type) {
        isMain = _isMain;
        propType = type;
        SetProp();
    }

    void SetProp()
    {
        switch (propType)
        {
            case Props.Bear:
                SelectProp(0);
                break;
            case Props.Feather:
                SelectProp(1);
                break;
            case Props.Flower:
                SelectProp(2);
                break;
            case Props.Honk:
                SelectProp(3);
                break;
            case Props.Spider:
                SelectProp(4);
                break;
            case Props.TorchLight:
                SelectProp(5);
                break;
            case Props.WalkingStick:
                SelectProp(6);
                break;

            default:
                SelectProp(0);
                break;
        }
    }
    void SelectProp(int index)
    {
        if (index < propHolder.childCount)
        {
            props[index].SetActive(true);
            currentProp = props[index].transform;
            currentPropRot = currentProp.localEulerAngles;
        }
    }
}

public enum Props
{
    Bear,
    Feather,
    Flower,
    Honk,
    Spider,
    TorchLight,
    WalkingStick
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SuspectController : MonoBehaviour
{
    [SerializeField] Suspect suspect;
    [SerializeField] Transform meshGRP;
    [SerializeField] float walkDuration = 2f;
    [SerializeField] GameObject ring;
    [Header("Debug : ")]
    [SerializeField] bool isGuilty = false;
    [SerializeField] SuspectAnimations guiltyAnim;
    [SerializeField] SuspectAnimations innocentAnim;
    [SerializeField] SuspectAnimations commonAnim;
    [SerializeField] int index;

    [SerializeField] List<Transform> chars;

    WaitForSeconds WAITTWO = new WaitForSeconds(2f);

    GameController gameController;
    SceneController sceneController;

    private void Awake()
    {
        chars = new List<Transform>();
        int max = meshGRP.childCount;
        for (int i = 0; i < max; i++)
        {
            chars.Add(meshGRP.GetChild(i));
        }
        //SetSuspect(5, false);
    }
    void Start()
    {
        ring.SetActive(false);
        gameController = GameController.GetController();
        sceneController = gameController.GetSceneController();
    }

    public Suspect GetSuspect() { return suspect; }
    public void SetSuspect(int characterIndex, int _index,SuspectAnimations _guilty, SuspectAnimations _innocent, SuspectAnimations _common)
    {
        index = _index;
        guiltyAnim = _guilty;
        innocentAnim = _innocent;
        commonAnim = _common;

        int max = meshGRP.childCount;
        for (int i = 0; i < max; i++)
        {
            chars[i].gameObject.SetActive(false);
        }
        chars[characterIndex].gameObject.SetActive(true);
        suspect = chars[characterIndex].GetComponent<Suspect>();
    }
    public void SetGuilty()
    {
        isGuilty = true;
    }

    public void PlayAnimation(SuspectAnimations anims)
    {
        StartCoroutine(AnimationRoutine(anims));
    }
    IEnumerator AnimationRoutine(SuspectAnimations anims)
    {
        suspect.PlayAnimation(anims);
        yield return WAITTWO;
        suspect.PlayAnimation(SuspectAnimations.IDLE);
    }
    public bool IsGuilty() { return isGuilty; }


    public void StartInteraction(bool _isMain)
    {
        if (_isMain)
        {
            if (isGuilty)
            {
                PlayAnimation(guiltyAnim);
            }
            else
            {
                PlayAnimation(innocentAnim);
            }
        }
        else
        {
            PlayAnimation(commonAnim);
        }

        sceneController.HighlightSuspect(index);
    }
    public int GetIndex() { return index; }
    public void WalkTo(Vector3 target)
    {
        suspect.PlayAnimation(SuspectAnimations.WALK);

        Vector3 right = new Vector3(0f,90f,0f);
        transform.DOLocalRotate(right, 0.3f);
        transform.DOMove(target, walkDuration).SetEase(Ease.OutSine).OnComplete(LookAtCamera);
    }
    public void SelectSuspect() { ring.SetActive(true); }
    public void DeselectSuspect() { ring.SetActive(false); }
    void LookAtCamera()
    {
        Vector3 front = new Vector3(0f, 180f, 0f);
        transform.DOLocalRotate(front, 0.3f).OnComplete(EnableInput);

        suspect.PlayAnimation(SuspectAnimations.IDLE);
    }
    void EnableInput() {
        gameController.GetInput().EnableInput();
    }
}

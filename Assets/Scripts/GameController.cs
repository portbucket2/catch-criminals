using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;

    [SerializeField] SceneController sceneController;
    [SerializeField] InputController inputController;
    [SerializeField] LightController lightController;
    [SerializeField] UIController uiController;
    [SerializeField] Camera mainCamera;

    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController() { return gameController; }


    public InputController GetInput() { return inputController; }
    public SceneController GetSceneController() { return sceneController; }
    public LightController GetLightController() { return lightController; }
    public UIController GetUI() { return uiController; }
    public Camera GetCamera() { return mainCamera; }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

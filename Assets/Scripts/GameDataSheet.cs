﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GameDataSheet", order = 1)]
public class GameDataSheet : ScriptableObject
{
    public List<LevelData> levelDatas;

    private void Awake()
    {
               
    }

    
}


[System.Serializable]
public struct LevelData
{
    public GameObject propPrefab;
    public Props mainProp;
    public Props extraProp;
    public string levelHint;
    public SuspectAnimations guiltyAnimation;
    public SuspectAnimations innocentAnimation;
    public SuspectAnimations commonAnimation;
}